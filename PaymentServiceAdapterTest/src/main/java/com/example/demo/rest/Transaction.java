package com.example.demo.rest;

import java.math.BigDecimal;


/**
 * Resource for transfer of money
 *
 * @author  Alexander Hansen Kappel, s182861
 * @version 1.0
 * @since   23-01-2019
 */


public class Transaction {
// Class Declaration
        // Instance Variables
        String Customer;
        String Merchant;
        BigDecimal amount;

    public Transaction() {

    }
        // Constructor Declaration of Class
        public Transaction(String Customer, String Merchant, BigDecimal amount)
        {
            this.Customer = Customer;
            this.Merchant = Merchant;
            this.amount = amount;
        }

        public String getCustomer()
        {
            return Customer;
        }
        public String getMerchant()
        {
            return Merchant;
        }

        public BigDecimal getAmount()
        {
            return amount;
        }

    public void setCustomer(String customer) {
        Customer = customer;
    }

    public void setMerchant(String merchant) {
        Merchant = merchant;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

