package com.example.demo.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Resource for transfer of money
 *
 * @author  Alexander Hansen Kappel, s182861
 * @version 1.0
 * @since   23-01-2019
 */

public class PaymentServiceAdapterTest {
	Client client = ClientBuilder.newClient();
	WebTarget r;

	/*
	String cpr = "123456-7890";
	String cpr2 = "012345-6789";
	*/

	@Test
	public void testNewAccountCustomer() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8083/payment");

		Transaction trans = new Transaction();
		trans.setAmount(new BigDecimal(200));
		trans.setCustomer("200220-2022");
		trans.setMerchant("110111-1011");

		/*
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("toCpr", "Test");
		jsonObj.put("fromCpr", "Person");
		jsonObj.put("amount", "200");
		*/

		//String combinedString = jsonObj.toString();
		Response result = r.request().post(Entity.entity(trans,"application/json"));

		System.out.println(result);
		assertNotNull(result);

		//System.out.println("requested tokens: " + result);
	}
}

