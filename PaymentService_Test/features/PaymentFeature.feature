Feature: Payment Feature

  Scenario: Transfer payment
    Given a merchant and a customer
    And that I have an minimum balance of 200
    When I request a transfer of money
    Then money is transferred
    And the accounts are deleted

  Scenario: Get account with CPR
    Given I am registered
    When I request to get the accountID
    Then I will get the accountID
		And the account are deleted

  Scenario: Bank service connection
    Given I am not connected to the bank
    When I try to establish a connection to the bank
    Then I am connected to the bank