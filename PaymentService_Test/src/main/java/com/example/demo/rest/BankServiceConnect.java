package com.example.demo.rest;

import java.math.BigDecimal;
import dtu.ws.fastmoney.*;

/**
* The class BankServiceConnect is the class that is call in the TransferResource
* and transfers money from one account to another account.
*
* @author  Kristian Krarup, s144122
* @version 1.0
* @since   22-01-2019
*/
public class BankServiceConnect {

    public BankServiceService service = new BankServiceService();
    public BankService port = service.getBankServicePort();


    /**
     * Creates a account on fastmoney with a amount of money.
     * @param user, such as a customer or merchant.
     * @param initBalance, the amount the account is created with.
     * @return accountId of the created account.
     */
    public String createAccountWithBalance(User user, BigDecimal initBalance) {
        // boolean createAccountSucces = false;
        String accountId = "";
        dtu.ws.fastmoney.User bankUser = new dtu.ws.fastmoney.User();
        bankUser.setCprNumber(user.getCpr().get());
        bankUser.setFirstName(user.getfirstName());
        bankUser.setLastName(user.getlastName());

        try {
            //System.out.println(initBalance);

            accountId = port.createAccountWithBalance(bankUser, initBalance);

            return accountId;
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        return accountId;

    }
    /**
     * Transfers money from one account to another account on fastmoney, using the SOAP classes
     * @param customerCPR, the CPR number of the account the money is taken from.
     * @param merchantCPR, the CPR number of the account the money is put in.
     * @param amount, the amount of money transferred.
     * @return a boolean that describes that the transfer is gone through or not
     */
	//make a transfer from the customer to the merchant
	public boolean Transfer(String customerCPR, String merchantCPR,BigDecimal amount) {
		String Customer;
		String Merchant;
		System.out.println("Transfer from bankservice with amount : " + amount);
		try {
			Customer = port.getAccountByCprNumber(customerCPR).getId();
			Merchant = port.getAccountByCprNumber(merchantCPR).getId(); 
			System.out.println("Balance on customer account : " + port.getAccountByCprNumber(customerCPR).getBalance());
			System.out.println("Balance on merchant account : " + port.getAccountByCprNumber(merchantCPR).getBalance());
			System.out.println("Got accountId's from cpr numbers");
			port.transferMoneyFromTo(Customer, Merchant,amount, "Payment");
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	/**
	 * Returns the port of BankServiceConnect BankService
	 * @return the port
	 */
    public BankService getPort() {
        return port;
    }
}
