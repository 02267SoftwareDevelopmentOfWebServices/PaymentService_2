package com.example.demo.rest;


public class CprNumber {

	private String value;


	public CprNumber(String value) throws Exception {
		
		if (value.length()!=11) {
			// CPR length exception
			throw new Exception(value);
		}
		
		if(value.contains("[a-zA-Z]+")){
			// CPR Only Numbers exception
			throw new Exception(value);
		}
		this.value = value;
	}
	
	public String get()  {
		return this.value;
	}

}
