package com.example.demo.rest;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import org.json.JSONObject;

/**
* Resource for transfer of money
*
* @author  Kristian Krarup, s144122
* @version 1.0
* @since   22-01-2019
*/
@Path("/payment")
public class TransferResource {

	// POST, because requestToken is not idempotent

	@POST
	@Consumes("application/json")
	@Produces("text/plain")
	public Response TransferPayment(Transaction transaction) {
        System.out.println("starter test...");
        String merchantCPR = transaction.getMerchant();
        String customerCPR = transaction.getCustomer();
        System.out.println("indlæser BigDecimal...");
        BigDecimal amount = transaction.getAmount();
        System.out.println("indlæser JSON object...");

		try {
			BankServiceConnect BankService = new BankServiceConnect();

            System.out.println("after bankservice...");

            boolean b = BankService.Transfer(customerCPR, merchantCPR, amount);

            System.out.println("transferring...");

            //JSONObject response = new JSONObject();
			//response.put("Transfer " + b);
			return Response.ok("Transfer " + b).build();

		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}	
}

