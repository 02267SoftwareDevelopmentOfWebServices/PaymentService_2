package com.example.demo.rest;

// Not used
public abstract class User {

	// Name name;
	// UniqueId uniqueId;
	String firstName;
	String lastName;
	CprNumber cprNumber;

	public User(String firstName, String lastName, CprNumber cprNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.cprNumber = cprNumber;

	}


	public CprNumber getCpr() {
		return this.cprNumber;
	}

	public abstract String getfirstName();

	public abstract String getlastName();

	public abstract void setAccountId(String accountId);
	
	public abstract String getAccountId();
	
	public abstract void setFirstName(String fName);
	
	public abstract void setLastName(String lName);
	



	//public abstractName getName(); // Do we need a name?
	//public IUser getType(); // Consumer or Merchant
	//public boolean isRegisteredToBank(); //
}
