package com.example.demo.rest;

public class Customer extends User{
	

	private String firstName, lastName;
	private CprNumber cprNumber;
	private String accountId;	
	
	public Customer(String firstName, String lastName, CprNumber cprNumber) {
		super(firstName, lastName, cprNumber);
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.cprNumber = cprNumber;
		
	}

	// Cpr Number
	@Override
	public CprNumber getCpr() {
		return this.cprNumber;
	}


	// Account ID ------------------------------------
	@Override
	public void setAccountId(String accountId) {
		
		this.accountId = accountId;
	}

	@Override
	public String getAccountId() {
		return this.accountId;
	}

	// First and Last name -------------------------------
	@Override
	public String getfirstName() {
		return this.firstName;
	}
	
	@Override
	public String getlastName() {
		
		return this.lastName;
	}

	@Override
	public void setFirstName(String fName) {
		this.firstName = fName;
		//BankServiceConnect.updateAccount(this);
	}

	@Override
	public void setLastName(String lName) {
		this.lastName = lName;
		//BankServiceConnect.updateAccount(this);
	}






	
}
