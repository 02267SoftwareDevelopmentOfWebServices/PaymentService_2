package demo.test;
import com.example.demo.rest.*;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.ws.fastmoney.Account;
/**
* This is the unit test, that has different scenarios that is described in PaymentFeature
*
* @author  Alexander Hansen Kappel, s182861
* @version 1.0
* @since   22-01-2019
*/

public class PaymentTest {
	BigDecimal initBalance;
	Merchant merchant;
	Customer customer;
	Customer customer1;
	BankServiceConnect BankService;
	BigDecimal balanceC1;
	BigDecimal balanceM1;
	BigDecimal balanceC2;
	BigDecimal balanceM2;
	String CPRm = "220119-0001";
	String CPRc = "220119-0002";
	String CPR = "010101-0000";

	@Before
	public void before() throws Exception {
		BankService = new BankServiceConnect();
		merchant = new Merchant("Lars", "Larsen", new CprNumber(CPRm));
		customer = new Customer("Hans", "Hansen", new CprNumber(CPRc));
		customer1 = new Customer("Test", "Test", new CprNumber(CPR));
		initBalance = new BigDecimal(500);
	}

	///////////////////////// Transfer payment//////////////////////////////////
	@Given("^a merchant and a customer$")
	public void aMerchantAndACustomer() throws Throwable {
		BankService.createAccountWithBalance(customer, initBalance);
		BankService.createAccountWithBalance(merchant, initBalance);
	}

	@Given("^that I have an minimum balance of (\\d+)$")
	public void thatIHaveAnInitialBalanceOf(BigDecimal balance) throws Throwable {
		Account C = BankService.port.getAccountByCprNumber(CPRc);
		Account M = BankService.port.getAccountByCprNumber(CPRm);
		balanceC1 = C.getBalance();
		balanceM1 = M.getBalance();
		int res = balanceC1.compareTo(balance);
		assertEquals(1,res);
	}


	@When("^I request a transfer of money$")
	public void iRequestAOf() throws Throwable {
		assertTrue(BankService.Transfer(CPRc,CPRm, new BigDecimal(200)));
	}

	
	@Then("^money is transferred$")
	public void moneyIsTransferred() throws Throwable {
		Account C = BankService.port.getAccountByCprNumber(CPRc);
		Account M = BankService.port.getAccountByCprNumber(CPRm);
		balanceC2 = C.getBalance();
		balanceM2 = M.getBalance();
		assertEquals(balanceM1.add(new BigDecimal(200)),balanceM2);
		assertEquals(balanceM1.subtract(new BigDecimal(200)),balanceC2);
	}
	
	@Then("^the accounts are deleted$")
	public void deleteAccounts()throws Throwable {
		BankService.port.retireAccount(BankService.port.getAccountByCprNumber(CPRc).getId());
		BankService.port.retireAccount(BankService.port.getAccountByCprNumber(CPRm).getId());
	}

	/////////////////////// Get account with CPR////////////////////////
	@Given("^I am registered$")
	public void iAm() throws Throwable {
		BankService.createAccountWithBalance(customer1, initBalance);
		assertNotNull(BankService.port.getAccountByCprNumber(CPR));
	}

	@When("^I request to get the accountID$")
	public void iRequestToGetAnAccount() throws Throwable {
		assertNotNull(BankService.port.getAccountByCprNumber(CPR));
	}

	@Then("^I will get the accountID$")
	public void iWillGetTheAccount() throws Throwable {
		assertNotNull(BankService.port.getAccountByCprNumber(CPR));

	}
	@Then("^the account are deleted$")
	public void deleteAccount()throws Throwable {
		BankService.port.retireAccount(BankService.port.getAccountByCprNumber(CPR).getId());
	}
	///////////////////////// Bank service connection /////////////////////////
	@Given("^I am not connected to the bank$")
	public void iAmNotConnectedToTheBank() throws Throwable {
		BankService = null;
		assertNull(BankService);
	}

	@When("^I try to establish a connection to the bank$")
	public void iTryToEstablishAConnectionToTheBank() throws Throwable {
		BankService = new BankServiceConnect();
	}

	@Then("^I am connected to the bank$")
	public void iAmConnectedToTheBank() throws Throwable {
		assertNotNull(BankService.getPort());
	}
}
