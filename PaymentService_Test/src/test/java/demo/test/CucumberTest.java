package demo.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
/**
* The class BankServiceConnect is the class that is call in the TransferResource
* 
* @author  Kristian Krarup, s144122
* @version 1.0
* @since   22-01-2019
*/
@RunWith(Cucumber.class)
@CucumberOptions(features="features",
		snippets=SnippetType.CAMELCASE)
public class CucumberTest {
}
